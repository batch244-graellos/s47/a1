const txtFirstname = document.querySelector("#txt-first-name");
const txtLastname = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

function updateSpanFullName() {
  spanFullName.innerHTML = txtFirstname.value + " " + txtLastname.value;
}

txtFirstname.addEventListener("input", updateSpanFullName);

txtLastname.addEventListener("input", updateSpanFullName);
